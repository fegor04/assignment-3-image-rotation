#pragma once
#include <stdio.h>

#include "../internal/image.h"

enum write_status {
  WRITE_OK = 0,
  WRITE_UNKNOWN_ERROR,
};

typedef enum write_status serializer(FILE *, const struct image *);

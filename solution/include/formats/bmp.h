#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "deserializer.h"
#include "formats/deserializer.h"
#include "formats/serializer.h"
#include "internal/pixel.h"
#pragma once

/**
 * Сохраняет изображение в формате BMP в указанный файл.
 */
serializer save_as_bmp;

/**
 * Читает изображение из bmp файла.
 * Если во время чтения произошла ошибка, то вся выделенная память будет
 * освобождена.
 */
deserializer read_from_bmp;

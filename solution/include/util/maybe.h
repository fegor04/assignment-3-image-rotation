#include <stdbool.h>
#include <string.h>

#include "internal/image.h"

struct maybe_image {
  struct image value;
  _Bool is_ok;
};

struct maybe_image some_image(const struct image value);

extern const struct maybe_image none_image;

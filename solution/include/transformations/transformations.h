#include "internal/image.h"
#include "util/maybe.h"

typedef struct maybe_image transformation(const struct image source);

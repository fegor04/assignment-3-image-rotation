#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "formats/bmp.h"
#include "formats/deserializer.h"
#include "formats/serializer.h"
#include "internal/image.h"
#include "transformations/rotation.h"

enum error_codes {
  ERROR_BAD_USAGE = 42,
  ERROR_COULD_NOT_READ,
  ERROR_COULD_NOT_WRITE,
  ERROR_TRANSFORMED_IMAGE_ALREADY_EXTSTS,
  ERROR_BAD_ANGLE,
  ERROR_BAD_MALLOC,
};

const size_t EXPECTED_NUMBER_OF_ARGS = 4;

/**
 * Проверяет входные аргументы программы.
 * В случае невалидных параметров выводит ошибку и завершает работу.
 * В случае успхеа записывает данные в память по соответствующим указателям.
 */
void parse_arguments(int argc, char **argv, FILE **input_file,
                     FILE **output_file, int32_t *angle);

int main(int argc, char **argv) {
  FILE *input_file = NULL;
  FILE *output_file = NULL;
  int32_t angle = 0;
  parse_arguments(argc, argv, &input_file, &output_file, &angle);

  struct image img;
  enum read_status read_status = read_from_bmp(input_file, &img);
  if (read_status != READ_OK) {
    fprintf(stderr, "Could not read BMP file. Error: %d", read_status);
    free(img.data);
    return ERROR_COULD_NOT_READ;
  }

  struct maybe_image rotated = some_image(img);
  for (int i = 0; i < angle / 90; i++) {
    if (rotated.is_ok) {
      struct pixel *old_image_data = rotated.value.data;
      rotated = image_transform_rotate(rotated.value);
      free(old_image_data);
    } else {
      perror("Could not allocate memory for image rotation.");
      return ERROR_BAD_MALLOC;
    }
  }

  enum write_status write_status = save_as_bmp(output_file, &rotated.value);
  if (write_status != WRITE_OK) {
    fprintf(stderr, "Could not write BMP file. Error code: %d", write_status);
    if (img.data != rotated.value.data) {
      free(rotated.value.data);
    }
    return ERROR_COULD_NOT_WRITE;
  }

  free(rotated.value.data);
  fclose(input_file);
  fclose(output_file);
  return 0;
}

void parse_arguments(int argc, char **argv, FILE **input_file,
                     FILE **output_file, int32_t *angle) {
  if (argc != EXPECTED_NUMBER_OF_ARGS) {
    fprintf(stderr,
            "Usage: ./image-transformer <source-image> "
            "<transformed-image> <angle>.\n");
    exit(ERROR_BAD_USAGE);
  }

  *angle = atoi(argv[3]);
  if (*angle < -270 || *angle > 270 || *angle % 90 != 0) {
    fprintf(stderr,
            "Wrong value of angle. Allowed values of angle are [-270, "
            "-180, -90, 0, 90, 180, 270].\n");
    exit(ERROR_BAD_ANGLE);
  }
  while (*angle < 0) {
    *angle = 360 + *angle;
  }

  *input_file = fopen(argv[1], "rb");
  if (*input_file == NULL) {
    fprintf(stderr, "Could not open input file %s.\n", argv[1]);
    exit(ERROR_COULD_NOT_READ);
  }

  *output_file = fopen(argv[2], "wb");
  if (*output_file == NULL) {
    fprintf(stderr, "Could not open output file %s.\n", argv[2]);
    fclose(*input_file);
    exit(ERROR_COULD_NOT_WRITE);
  }
}

#include "transformations/rotation.h"

struct maybe_image image_transform_rotate(const struct image source) {
  struct image result;
  result.width = source.height;
  result.height = source.width;
  result.data = malloc(sizeof(struct pixel) * source.width * source.height);
  if (result.data == NULL) {
    return none_image;
  }
  for (int i = 0; i < source.height; i++) {
    for (int j = 0; j < source.width; j++) {
      *(result.data + (result.height - j - 1) * result.width + (i)) =
          *(source.data + i * source.width + j);
    }
  }

  return some_image(result);
}
